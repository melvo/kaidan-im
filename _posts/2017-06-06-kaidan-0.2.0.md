---
layout: post
title: "Kaidan 0.2.0 released - Roster Editing, Kirigami 2 and much more"
date: 2017-06-06 20:08:00 +02:00
categories: release
author: lnj
---

## Changelog

User interface:
 * GUI: Port to Kirigami 2 (#81) (JBB)
 * Use Material/Green Theme per default (LNJ)
 * Login page: New design with diaspora* login option (#87) (JBB)
 * Chat page: Slightly improved design (LNJ)

New features:
 * Add Roster Editing (#84, #86) (LNJ, JBB)
 * Roster refreshes now automatically (#83) (LNJ)
 * Contacts are now sorted (LNJ)
 * Add unread message counters (#92, #101) (LNJ)
 * Add LibNotify-Linux notifications (#90) (LNJ)
 * Add custom JID resources (#82) (LNJ)
 * Add XEP-0184: Message Delivery Receipts (LNJ)
 * Disable stream compression by default (for HipChat/other server compatibility) (LNJ)

Fixes
 * AboutPage: Fix possible closing of multiple pages (LNJ)

**Download**: [kaidan-v0.2.0.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.2.0/kaidan-v0.2.0.tar.gz)
