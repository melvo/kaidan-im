---
layout: post
title: "Kaidan 0.1.0 released"
date: 2017-02-26 12:51:09 +01:00
author: lnj
---

There's not much to say: Kaidan is the new convergent XMPP client without features!

**Download**: [kaidan-v0.1.0.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.1.0/kaidan-v0.1.0.tar.gz)
