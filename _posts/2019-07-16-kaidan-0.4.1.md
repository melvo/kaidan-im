---
layout: post
title: "Kaidan 0.4.1 released!"
date: 2019-07-16 15:00:00 +02:00
author: zatrox
---
![screenshot of Kaidan 0.4]({{ "/images/screenshots/screenshot-0.4.png" | prepend: site.baseurl }})

After some problems were encountered in Kaidan 0.4.1, we tried to fix the most
urgent bugs.

### Changelog

* Fix SSL problems for AppImage (lnj)
* Fix connection problems (lnj)
* Keep QXmpp v0.8.3 compatibility (lnj)

### Download

* [Source code (.tar.xz)](https://download.kde.org/stable/kaidan/0.4.1/kaidan-0.4.1.tar.xz)
* [Linux AppImage (x64)](https://download.kde.org/stable/kaidan/0.4.1/kaidan-0.4.1.x86_64.appimage)
* [Windows (x64)](https://download.kde.org/stable/kaidan/0.4.1/kaidan-0.4.1.x86_64.exe)
* [macOS (x64)](https://download.kde.org/stable/kaidan/0.4.1/kaidan-0.4.1.x86_64.macos.tar)
* [Android (armv7) (experimental!)](https://download.kde.org/stable/kaidan/0.4.1/kaidan-0.4.1.armhf.apk)
* [Ubuntu Touch (armv7) (experimental!)](https://download.kde.org/stable/kaidan/0.4.1/kaidan-0.4.1.armhf.click)
* [Flatpak](https://flathub.org/apps/details/im.kaidan.kaidan)
