---
layout: page
title: Download
permalink: /download/
sitemap: true
---

### Stable release

Downloads for **Kaidan {{ site.latest_release }}**:

{% assign downloads = "downloads_" | append: site.latest_release | append: ".md" %}
{% include {{ downloads }} %}

### Nightly builds
#### Linux

 * [Nightly Flatpak](https://invent.kde.org/network/kaidan/-/wikis/using/flatpak)

#### Android (experimental)

* [KDE Nightly F-Droid repository](https://community.kde.org/Android/FDroid)
* [APK (ARMv7)](https://binary-factory.kde.org/job/Kaidan_Nightly_android-arm/lastSuccessfulBuild/artifact/kaidan-armeabi-v7a.apk) - mostly older smartphones
* [APK (ARMv8)](https://binary-factory.kde.org/job/Kaidan_Nightly_android-arm64/lastSuccessfulBuild/artifact/kaidan-arm64-v8a.apk) - mostly newer smartphones

### Source code

Kaidan's source code can be found at [invent.kde.org](https://invent.kde.org/network/kaidan).
