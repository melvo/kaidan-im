# Kaidan's Website

This repository contains the data for [Kaidan's website](https://kaidan.im).
[Kaidan](https://invent.kde.org/network/kaidan) is a cross-platform XMPP client.

## Development

The following steps are needed for development:

1. [Install Jekyll](https://jekyllrb.com/docs/installation/) for testing your changes on Kaidan's website locally. (On Debian and Ubuntu: `sudo apt install jekyll`)
1. Run `jekyll serve` in the root directory of this repository to start a local web server.
1. Open http://127.0.0.1:4000 in your web browser to see your local changes.
1. Modify the sources you want to.
1. Reload the website to see your changes.
1. Enter Ctrl+C to stop the web server.
1. Submit your changes.

## Creating a New Post

The following steps are needed to created a new post:

1. Duplicate the template file *_posts/20YY-MM-DD-short-name.md* and rename it accordingly (e.g. *_/posts/2020-01-10-quick-onboarding.md*).
1. Modify the title by using the title case.
1. Adjust the date in the given format and pay attention to the correct time zone.
1. Add all authors separated like in the template.
1. Use one line per sentence and an empty line to separate paragraphs.
1. Add images for embedding them in the post to the *images* directory (e.g. *images/screenshots/2020-01-10-quick-onboarding.png*).

New posts are automatically announced in the *News* section on the specified date.

## Deployment

As soon as your commit is in the [master branch of the main remote repository](https://invent.kde.org/websites/kaidan-im/tree/master), the new version of Kaidan's website is deployed and you can see your changes online.
