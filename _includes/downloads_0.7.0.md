* [Source code (.tar.xz)](https://download.kde.org/unstable/kaidan/0.7.0/kaidan-0.7.0.tar.xz) ([sig](https://download.kde.org/unstable/kaidan/0.7.0/kaidan-0.7.0.tar.xz.sig) signed with [4663231A91A1E27B](https://keys.openpgp.org/vks/v1/by-fingerprint/03C2D10DC97E5B0BEBB8F3B44663231A91A1E27B))
* [Windows Installer x64 (.exe)](https://download.kde.org/unstable/kaidan/0.7.0/kaidan-0.7.0-msvc2019_64-cl.exe)
* [Linux (Flatpak on Flathub)](https://flathub.org/apps/details/im.kaidan.kaidan)

Or install Kaidan from your distribution:

<a href="https://repology.org/project/kaidan/versions">
    <img src="https://repology.org/badge/vertical-allrepos/kaidan.svg?columns=4" alt="Packaging status">
</a>
